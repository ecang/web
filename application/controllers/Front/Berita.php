<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Berita extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Template', 'template');
        $this->load->model('Admin/Berita_model', 'Berita');
        $this->load->model('Admin/Kelurahan_model', 'Kelurahan');
    }
    public function index()
    {
        $berita = $this->Berita->order_by('tanggal_berita', 'DESC')->get_all();
        $kelurahan = $this->Kelurahan->order_by('nama_kelurahan', 'ASC')->get_all();
        $data['content'] = 'Frontend/Berita/index';
        $data['kelurahan'] = $kelurahan;
        $data['berita'] = $berita;
      // $data['title'] = 'Dashboard';
        $this->template->front_template($data);
    }

    public function detail($id)
    {

        $data['content'] = 'Frontend/Berita/detail';
        $data['detail'] = $this->Berita->get($id);
        $this->template->front_template($data);
    }
}

/* End of file Berita.php */
