<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Fasilitas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Profile_model', 'Profile');
        $this->load->model('Admin/Kelurahan_model', 'Kelurahan');
    }

    public function index()
    {
        $data = array(
            'section' => 'kelurahan/kelurahan',
            'script' => array('kelurahan.js'),
        );
        $this->load->view('Admin/theme', $data);
    }
    public function add()
    {
        $data = array(
            'section' => 'kelurahan/kelurahan_add',
            'script' => array('kelurahan.js'),
        );
        $this->load->view('Admin/theme', $data);
    }
    public function edit()
    {
        $data = array(
            'section' => 'kelurahan/kelurahan_add',
            'script' => array('berita.js'),
        );
        $this->load->view('Admin/theme', $data);
    }
    public function getTable()
    {
        $filterd = $this->Kelurahan->count_rows();
        $all = $this->Kelurahan->get_all();
        $output = array();
        $no = 1;
        if ($filterd > 0) {
            foreach ($all as $key => $value) {
                $button = "";
                $button .= '<a class="btn btn-primary btn-xs" href="' . base_url('AdminPage/Page/edit/' . $value->id_kelurahan) . '" title="Edit"><i class="fa fa-pencil"></i></a>';
                $button .= '<button class="btn btn-danger btn-xs" data-val="' . $value->id_kelurahan . '"  id="delBtn"><i class="fa fa-trash"></i></button>';
                $data = array();
                $data[] = $no++;
                $data[] = $value->nama_kelurahan;
                $data[] = $value->alamat_kelurahan;
                $data[] = $value->nama_lurah;
                $data[] = $button;
                $output[] = $data;
            }
        }
        echo json_encode(
            array(
                'data' => $output,
            )
        );
    }
    public function save()
    {
        $method = $this->input->post('method');
        $upload = $this->_upload();
        $valid = $this->validation_check();
        $valid = true;
        if ($upload == false || $valid !== true) {
            if ($method == 'update') {
                $id = $this->input->post('id_speakers');
                $this->edit($id);
            } else {
                $this->add();
            }
        } else {
            $c_slug = slugify($this->input->post('nama_kelurahan'));
            $getSlug = $this->Kelurahan->where('slug', $c_slug)->count_rows();
            if ($getSlug > 0) {
                $slugrow =  $this->Kelurahan->where('slug', $c_slug)->get();
                $slug = $slugrow->slug . '-' . $getSlug;
            } else {
                $slug = $c_slug;
            }
            $data = array(
                'nama_kelurahan' => $this->input->post('nama_kelurahan'),
                'alamat_kelurahan' => $this->input->post('alamat_kelurahan'),
                'website' => $this->input->post('website'),
                'nama_lurah' => $this->input->post('nama_lurah'),
                'nip_lurah' => $this->input->post('nip_lurah'),
                'foto' => $this->input->post('foto'),
                'keterangan' => $this->input->post('keterangan'),
                'keterangan_foto' => $this->input->post('keterangan_foto'),
                'slug' => $slug,
                'created_by' => $this->session->userdata('id_petugas'),
            );
            if ($method == 'update') {
                $id = $this->input->post('id_speakers');
                $row = $this->Kelurahan->get($id);
                if (empty($_FILES['foto']['name'])) {
                    $save = $this->Kelurahan->update($data, $id);
                    $this->session->set_flashdata('success_message', 'Berhasil Mengubah data');
                } else {
                    unlink('./assets/img/kelurahan/' . $row->gambar_berita);
                    $data['foto'] = $upload['file_name'];
                    $save = $this->Kelurahan->update($data, $id);
                    $this->session->set_flashdata('success_message', 'Berhasil Menyimpan data');
                }
            } else {
                $data['foto'] = $upload['file_name'];
                $save = $this->Kelurahan->insert($data);
            }
            if ($save == false) {
                $this->session->set_flashdata('valid_message', 'Gagal Menyimpan data');
                $this->add();
            } else {
                redirect(base_url('Admin/Kelurahan'), 'refresh');
            }
        }
    }
    private function _upload()
    {
        if (!is_dir('./assets/img/kelurahan/')) {
            mkdir('./assets/img/kelurahan/', 0700, TRUE);
        }
        $config['upload_path']          = './assets/img/kelurahan/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 5000;
        $config['file_name']            = 'kelurahan-' . date('Ymdhis');
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        //allow overwrite name files
        $this->upload->display_errors('<p class="text-danger">', '</p>');
        $this->upload->overwrite = true;

        if (!$this->upload->do_upload('foto')) {
            $err_mess = '<strong>Image field</strong> :' . $this->upload->display_errors();
            $this->session->set_flashdata('valid_message', $err_mess);
            return false;
        } else {
            $data = $this->upload->data();
            return $data;
        }
    }
    function validation_check()
    {
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('nama_kelurahan', 'Nama Kelurahan', 'trim|required');
        $this->form_validation->set_rules('alamat_kelurahan', 'Alamat Kelurahan', 'trim|required');
        $this->form_validation->set_rules('website', 'Tags', 'trim');
        $this->form_validation->set_rules('nama_lurah', 'Nama Lurah', 'trim|required');
        $this->form_validation->set_rules('keterangan_foto', 'Keterangan', 'trim');
        $this->form_validation->set_rules('nip_lurah', 'NIP', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $err_mess = '';
            foreach ($_POST as $key => $value) {
                $err_mess .=  form_error($key);
            }
            $this->session->set_flashdata('valid_message', $err_mess);
        } else {
            return true;
        }
    }
}

/* End of file Fasilitas.php */
