<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Berita extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Profile_model', 'Profile');
        $this->load->model('Admin/Berita_model', 'Berita');
        $this->load->model('Template', 'template');
    }

    public function index()
    {
        // $data = array(
        //     'section' => 'berita/berita',
        //     'script' => array('berita.js'),
        // );
        // $this->load->view('Admin/theme', $data);
        $data['content'] = 'Backend/Berita/berita';
        $data['title'] = 'Berita Website';
        $data['s_title'] = 'Tabel';
        $data['script'] = ['berita.js'];
        $this->template->back_template($data);
    }
    public function add()
    {
        $data['content'] = 'Backend/Berita/berita_add';
        $data['title'] = 'Berita Website';
        $data['s_title'] = 'Tambah';
        $data['script'] = ['berita.js'];
        $this->template->back_template($data);
    }
    public function edit($id)
    {

        $data['content'] = 'Backend/Berita/berita_edit';
        $data['edit'] = $this->Berita->get($id);
        $data['title'] = 'Berita Website';
        $data['s_title'] = 'Edit';
        $data['script'] = ['berita.js'];
        $this->template->back_template($data);
    }
    public function getTable()
    {
        $list = $this->Berita->get_datatables();
        $filterd = $this->Berita->count_filtered();
        $output = array();
        $no = $this->input->post('start');
        if ($filterd > 0) {
            foreach ($list as $key => $value) {
                $button = "";
                $button .= '<a class="btn btn-primary btn-xs" href="' . base_url('Admin/Berita/edit/' . $value->id_berita) . '" title="Edit"><i class="fas fa-pencil-ruler"></i></a>';
                $button .= '<button class="btn btn-danger btn-xs" data-val="' . $value->id_berita . '"  id="delBtn"><i class="fa fa-trash"></i></button>';
                $no++;
                $data = array();
                $data[] = $no;
                $data[] = $value->judul_berita;
                $data[] = $value->tanggal_berita;
                $data[] = $button;
                $output[] = $data;
            }
        }
        echo json_encode(
            array(
                'data' => $output,
                "draw" => $_POST['draw'],
                "recordsTotal" => $this->Berita->count_all(),
                "recordsFiltered" => $this->Berita->count_filtered()
            )
        );
    }
    public function save()
    {
        $method = $this->input->post('method');
        $upload = $this->_upload();
        $valid = $this->validation_check();
        if ($upload == false || $valid !== true) {
            if ($method == 'update') {
                $id = $this->input->post('id_speakers');
                $this->edit($id);
            } else {
                $this->add();
            }
        } else {
            $c_slug = slugify($this->input->post('judul_berita'));
            $getSlug = $this->Berita->where('slug', $c_slug)->count_rows();
            if ($getSlug > 0) {
                $slugrow =  $this->Berita->where('slug', $c_slug)->get();
                $slug = $slugrow->slug . '-' . $getSlug;
            } else {
                $slug = $c_slug;
            }
            $data = array(
                'judul_berita' => $this->input->post('judul_berita'),
                'tags_berita' => implode(',',  $this->input->post('tags_berita')),
                'isi_berita' => $this->input->post('isi_berita'),
                'tanggal_berita' => $this->input->post('tanggal_berita'),
                'slug' => $slug,
                'created_by' => $this->session->userdata('id_petugas'),
            );
            if ($method == 'update') {
                $id = $this->input->post('id_speakers');
                $row = $this->Berita->get($id);
                if (empty($_FILES['gambar_berita']['name'])) {
                    $save = $this->Berita->update($data, $id);
                    $this->session->set_flashdata('success_message', 'Berhasil Mengubah data');
                } else {
                    unlink('./assets/img/berita/' . $row->gambar_berita);
                    $data['gambar_berita'] = $upload['file_name'];
                    $save = $this->Berita->update($data, $id);
                    $this->session->set_flashdata('success_message', 'Berhasil Menyimpan data');
                }
            } else {
                $data['gambar_berita'] = $upload['file_name'];
                $save = $this->Berita->insert($data);
            }
            if ($save == false) {
                $this->session->set_flashdata('valid_message', 'Gagal Menyimpan data');
                $this->add();
            } else {
                redirect(base_url('Admin/Berita'), 'refresh');
            }
        }
    }
    private function _upload()
    {
        if (!is_dir('./assets/img/berita/')) {
            mkdir('./assets/img/berita/', 0700, TRUE);
        }
        $config['upload_path']          = './assets/img/berita/';
        $config['allowed_types']        = 'jpg|png';
        $config['max_size']             = 5000;
        $config['file_name']            = 'berita-' . date('Ymdhis');
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);
        //allow overwrite name files
        $this->upload->display_errors('<p class="text-danger">', '</p>');
        $this->upload->overwrite = true;

        if (!$this->upload->do_upload('gambar_berita')) {
            $err_mess = '<strong>Image field</strong> :' . $this->upload->display_errors();
            $this->session->set_flashdata('valid_message', $err_mess);
            return false;
        } else {
            $data = $this->upload->data();
            return $data;
        }
    }
    function upload_image()
    {
        if (isset($_FILES["image_summernote"]["name"])) {
            $this->load->library('upload');
            if (!is_dir('./assets/img/berita/content/')) {
                mkdir('./assets/img/berita/content/', 0700, TRUE);
            }
            $config['upload_path'] = './assets/img/berita/content/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['file_name']            = 'berita-' . date('Ymdhis');
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('image_summernote')) {
                $this->upload->display_errors();
                echo $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                //Compress Image
                $config['image_library'] = 'gd2';
                $config['source_image'] = './assets/img/berita/content/' . $data['file_name'];
                $config['create_thumb'] = FALSE;
                $config['maintain_ratio'] = TRUE;
                $config['quality'] = '60%';
                $config['width'] = 800;
                $config['height'] = 800;
                $config['new_image'] = './assets/img/berita/content/' . $data['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                echo base_url() . 'assets/img/berita/content/' . $data['file_name'];
            }
        }
        //Delete image summernote

    }
    function delete_image()
    {
        $src = $this->input->post('src');
        $file_name = str_replace(base_url(), '', $src);
        if (unlink($file_name)) {
            echo 'File Delete Successfully';
        }
    }
    function validation_check()
    {
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('judul_berita', 'Judul', 'trim|required');
        $this->form_validation->set_rules('isi_berita', 'Isi Berita', 'trim|required');
        // $this->form_validation->set_rules('tags_berita', 'Tags', 'trim|required');
        $this->form_validation->set_rules('tanggal_berita', 'Tanggal', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $err_mess = '';
            foreach ($_POST as $key => $value) {
                $err_mess .=  form_error($key);
            }
            $this->session->set_flashdata('valid_message', $err_mess);
        } else {
            return true;
        }
    }
}

/* End of file Berita.php */
