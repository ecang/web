<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Template', 'template');
        $this->load->model('Admin/Menu_model', 'Menu');
        $this->load->model('Admin/Sub_Menu_model', 'subMenu');
    }

    public function index()
    {
        $data['content'] = 'Backend/Menu/menu';
        $data['title'] = 'Menu Website';
        $data['s_title'] = 'Tabel';
        $this->template->back_template($data);
    }
    public function add()
    {
        $data['content'] = 'Backend/Menu/menu_add';
        $data['menu'] = $this->Menu->get_all();
        $data['title'] = 'Menu Website';
        $data['s_title'] = 'Tambah';
        $this->template->back_template($data);
    }
    public function edit($id)
    {
        $data['content'] = 'Backend/Menu/menu_edit';
        $data['edit'] = $this->Menu->get($id);
        $data['title'] = 'Menu Website';
        $data['s_title'] = 'Edit';
        $data['menu'] = $this->Menu->get_all();
        $this->template->back_template($data);
    }
    public function getTable()
    {

        $filterd = $this->Menu->count_rows();
        $all = $this->Menu->get_all();
        $output = array();
        $no = 1;
        if ($filterd > 0 ) {
            foreach ($all as $key => $value) {
                $submenu = $this->Menu->get($value->id_parent);
                if ($value->id_menu != 0) {
                    // $count_sub = $this->Menu->where('id_menu', $value->id_menu)->count_rows();                    
                    $button = "";
                    $button .= '<a class="btn btn-primary btn-xs" href="' . base_url('Admin/Menu/edit/' . $value->id_menu) . '" title="Edit"><i class="fas fa-pencil-ruler"></i></a>';
                    $button .= '<button class="btn btn-danger btn-xs" data-val="' . $value->id_menu . '"  id="delBtn" title="Hapus"><i class="fa fa-trash"></i></button>';
                        
                    $data = array();
                    $data[] = $no++;
                    $data[] = $value->nama_menu;
                    $data[] = $value->link;
                    $data[] = $submenu->nama_menu;
                    $data[] = $button;
                    $output[] = $data;
                }
                
            }
        }

        echo json_encode(
            array(
                'data' => $output,
            )
        );
    }

    public function save()
    {
        $method = $this->input->post('method');
        $valid = $this->validation_check();
        $valid = true;
        
            $c_slug = slugify($this->input->post('nama_menu'));
            $getSlug = $this->Menu->where('slug', $c_slug)->count_rows();
            if ($getSlug > 0) {
                $slugrow =  $this->Menu->where('slug', $c_slug)->get();
                $slug = $slugrow->slug . '-' . $getSlug;
            } else {
                $slug = $c_slug;
            }
            $data = array(
                'nama_menu' => $this->input->post('nama_menu'),
                'id_parent' => $this->input->post('id_parent'),
                'link' => $this->input->post('link'),
                'ket_menu' => $this->input->post('ket_menu'),
                'slug' => $slug,
            );
            if ($method == 'update') {
                $id = $this->input->post('id_menu');
                $row = $this->Menu->get($id);
                $save = $this->Menu->update($data, $id);
                $this->session->set_flashdata('success_message', 'Berhasil Menyimpan data');
               
            } else {
                $save = $this->Menu->insert($data);
                $this->session->set_flashdata('success_message', 'Berhasil Menyimpan data');
            }
            if ($save == false) {
                $this->session->set_flashdata('valid_message', 'Gagal Menyimpan data');
                $this->add();
            } else {
                redirect(base_url('Admin/Menu'), 'refresh');
            }
        
    }

    function validation_check()
    {
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        $this->form_validation->set_rules('nama_menu', 'Nama Menu', 'trim|required');
        $this->form_validation->set_rules('id_parent', 'Sub Menu', 'trim|required');
        $this->form_validation->set_rules('link', 'Link Menu', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $err_mess = '';
            foreach ($_POST as $key => $value) {
                $err_mess .=  form_error($key);
            }
            $this->session->set_flashdata('valid_message', $err_mess);
        } else {
            return true;
        }
    }
}

/* End of file Menu.php */
