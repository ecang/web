<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admin/Login_model', 'Login');
        $this->load->library('encryption');
    }


    public function index()
    {

        $this->load->view('Admin/login');
    }
    public function prosesLogin()
    {
        $username = $this->input->post('username');
        $pass = $this->input->post('password');

        $getLogin = $this->Login->loginProses($username, $pass);
        if ($getLogin != FALSE) {
            $data_session = array(
                'logged' => true,
                'username' => $getLogin->username,
                'id_petugas' => $getLogin->id_admin,
                'nama_petugas' => $getLogin->nama_petugas,
                'status_petugas' => $getLogin->status_petugas,
            );
            $this->session->set_userdata($data_session);
            $response = array(
                'status' => true,
                'message' => 'Login Berhasil',
            );
            $this->session->set_flashdata($response);
            redirect(base_url('Dashboard'));
        } else {
            $response = array(
                'status' => false,
                'error' => 'Invalid username or password'
            );
            $this->session->set_flashdata($response);
            redirect(base_url('Login'));
        }
    }
    public function create($pass)
    {
        $password = $this->encryption->encrypt($pass);
        echo $password;
    }
    public function logout()
    {
        $this->session->sess_destroy();

        redirect(base_url('Login'));
    }
}

/* End of file Login.php */
