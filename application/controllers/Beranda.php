<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends CI_Controller
  {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Template', 'template');
        $this->load->model('Admin/Berita_model', 'Berita');
        $this->load->model('Admin/Kelurahan_model', 'Kelurahan');
        $this->load->model('M_menu', 'Menu');
    }

    function index()
    {
        $berita = $this->Berita->limit(3)->order_by('tanggal_berita', 'DESC')->get_all();
        $kelurahan = $this->Kelurahan->order_by('nama_kelurahan', 'ASC')->get_all();
        $data['content'] = 'Frontend/Beranda/index';
        $data['kelurahan'] = $kelurahan;
        $data['berita'] = $berita;
        // $data['menu'] = $this->Menu->getMenu(0,"");
      // $data['title'] = 'Dashboard';
        $this->template->front_template($data);
    }
  }

?>
