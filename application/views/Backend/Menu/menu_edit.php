<div class="main-content">
	<section class="section">
		<div class="section-header">
			<h1><?= (!empty($title) ? $title : 'Data') ?></h1>
			<div class="section-header-breadcrumb">
				<div class="breadcrumb-item active"><a href="<?= base_url() ?>">Dashboard</a></div>
				<div class="breadcrumb-item"><a href="<?= base_url('Admin/Menu') ?>">Menu</a></div>
				<div class="breadcrumb-item">Edit</div>
			</div>
		</div>

		<div class="section-body">
			<h2 class="section-title"><?= (!empty($title) ? $title : 'Data') ?></h2>
			<p class="section-lead">
				<?= (!empty($s_title) ? $s_title : 'Data') ?>
			</p>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-12">
					<div class="card">

						<form method="POST" id="form-event" action="<?= base_url('Admin/Menu/save') ?>"
							enctype="multipart/form-data">

							<div class="card-body">
								<?php if ($this->session->flashdata('valid_message')) : ?>
								<div class="alert alert-danger">
									<?= $this->session->flashdata('valid_message') ?>
								</div>
								<?php endif ?>
								<input type="hidden" class="form-control col-md-4" name="method" id="method" value="update">
								<input type="hidden" class="form-control col-md-4" name="id_menu" id="id_menu" value="<?php echo $edit->id_menu; ?>">
								<div class="form-group">
									<label>Nama Menu</label>
									<input type="text" class="form-control col-md-4" name="nama_menu" id="nama_menu"
										value="<?php echo (set_value('nama_menu')) ? set_value('nama_menu') : $edit->nama_menu; ?>">
								</div>
								<div class="form-group">
									<label>Sub Menu</label>
									<select class="form-control select2" name="id_parent" id="id_parent">
										<?php foreach ($menu as $r) { ?>
										<option
											<?php if($r->id_menu == $edit->id_parent){ echo 'selected="selected"'; } ?>
											value="<?php echo $r->id_menu ?>"><?php echo $r->nama_menu?> </option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label>Link Menu</label>
									<input type="text" class="form-control" name="link" id="link"
										value="<?php echo (set_value('link')) ? set_value('link') : $edit->link; ?>">
								</div>
								<div class="form-group">
									<label>Keterangan Menu</label>
									<textarea name="ket_menu" class="form-control" id="ket_menu"
										style="height: 90px"><?php echo (set_value('ket_menu')) ? set_value('ket_menu') : $edit->ket_menu; ?></textarea>
								</div>
							</div>
							<div class="card-footer">
								<button class="btn btn-primary">Simpan</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
