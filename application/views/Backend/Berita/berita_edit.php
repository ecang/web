<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= (!empty($title) ? $title : 'Data') ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?= base_url() ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="<?= base_url('Admin/Berita') ?>">Berita</a></div>
                <div class="breadcrumb-item">Tambah</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title"><?= (!empty($title) ? $title : 'Data') ?></h2>
            <p class="section-lead">
                <?= (!empty($s_title) ? $s_title : 'Data') ?>
            </p>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">

                        <form method="POST" id="form-event" action="<?= base_url('Admin/Berita/save') ?>" enctype="multipart/form-data" onsubmit="return postForm()">

                            <div class="card-body">
                                <?php if ($this->session->flashdata('valid_message')) : ?>
                                    <div class="alert alert-danger">
                                        <?= $this->session->flashdata('valid_message') ?>
                                    </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label>Judul Berita</label>
                                    <input type="text" class="form-control col-md-4" name="judul_berita" id="judul_berita" value="<?php echo (set_value('judul_berita')) ? set_value('judul_berita') : $edit->judul_berita; ?>">
                                    
								<input type="hidden" class="form-control col-md-4" name="id_berita" id="id_berita" value="<?php echo $edit->id_berita; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Isi Berita</label>
                                    <textarea class="summernote-berita" name="isi_berita" id="isi_berita" data-uploaduri="<?= base_url('Admin/Berita/') ?>">
									<?php echo (set_value('isi_berita')) ? set_value('isi_berita') : $edit->isi_berita; ?></textarea>
                                    <div style="display: none" id="data-calback"></div>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Berita</label>
                                    <input type="datetime" name="tanggal_berita" id="tanggal_berita" class="form-control datetimepicker" value="<?php echo (set_value('tanggal_berita')) ? set_value('tanggal_berita') : $edit->tanggal_berita; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Tags</label>
                                    <select class="form-control tagsberita" multiple="" name="tags_berita[]" data-url="<?= base_url('Tags_berita') ?>">
                                        <option></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Gambar Utama</label>
                                    <div class="col-sm-12 ">
                                        <div id="image-preview" class="image-preview" style="background-image: url('<?php echo set_value('foto_anggota') ?>')">
                                            <label for=" image-upload" id="image-label">Pilih File</label>
                                            <input type="file" name="gambar_berita" id="image-upload" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>