<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Blank Page</h1>
        </div>

        <div class="section-body">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                <div class="project-wrapper">
                    <div class="project">
                        <div class="photo-wrapper">
                            <div class="photo">
                                <a class="fancybox" href="<?= base_url('assets/img/gallery/' . $id_album . '/' . $value->photo_image) ?>" alt=""><img class="img-responsive" src="<?= base_url('assets/img/gallery/' . $id_album . '/' . $value->photo_image) ?>" alt="<?= $value->photo_alt ?>"></a>
                            </div>
                            <div class="overlay"></div>
                        </div>
                        <div class="text-center">
                            <div class="btn-group">
                                <a type="button" href="<?= base_url('AdminPage/Gallery/edit_photo/' . $value->id_photo) ?>" class="btn btn-default">Edit</a>
                                <button type="button" class="btn btn-default" onclick="delphoto('<?= $value->id_photo ?>')">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>