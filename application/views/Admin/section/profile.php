<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1><?= (!empty($title) ? $title : 'Data') ?></h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="<?= base_url() ?>">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="<?= base_url('Anggota') ?>">Anggota</a></div>
                <div class="breadcrumb-item">Tambah</div>
            </div>
        </div>

        <div class="section-body">
            <h2 class="section-title"><?= (!empty($title) ? $title : 'Data') ?></h2>
            <p class="section-lead">
                <?= (!empty($s_title) ? $s_title : 'Data') ?>
            </p>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">

                        <form method="POST" id="form-event" action="<?= base_url('Admin/Profile/save') ?>" enctype="multipart/form-data">

                            <div class="card-body">
                                <?php if ($this->session->flashdata('valid_message')) : ?>
                                    <div class="alert alert-danger">
                                        <?= $this->session->flashdata('valid_message') ?>
                                    </div>
                                <?php endif ?>
                                <div class="form-group">
                                    <label>Profile Web Nama</label>
                                    <input type="text" class="form-control col-md-4" name="profil_nama" id="profil_nama" value="<?php echo (set_value('profil_nama')) ? set_value('profil_nama') : $profile->profil_nama; ?>">
                                </div>
                                <div class="form-group">
                                    <label>No Telpon</label>
                                    <input type="text" class="form-control" name="no_telpon" id="no_telpon" value="<?php echo (set_value('no_telpon')) ? set_value('no_telpon') : $profile->no_telpon; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" id="email" value="<?php echo (set_value('email')) ? set_value('email') : $profile->email; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Alamat</label>
                                    <textarea name="alamat" class="form-control" id="alamat" style="height: 90px"><?php echo (set_value('alamat')) ? set_value('alamat') : $profile->alamat; ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Waktu Pelayanan</label>
                                    <textarea name="w_pelayanan" class="form-control" id="w_pelayanan" style="height: 90px"><?php echo (set_value('w_pelayanan')) ? set_value('w_pelayanan') : $profile->w_pelayanan; ?></textarea>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label>Logo Baru</label>
                                        <div class="col-sm-12 ">
                                            <div id="image-preview" class="image-preview" style="background-image: url('<?php echo set_value('foto_anggota') ?>')">
                                                <label for=" image-upload" id="image-label">Pilih File</label>
                                                <input type="file" name="logo" id="image-upload" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Logo Digunakan</label>
                                        <div class="col-sm-12 ">
                                            <div id="image-previews" class="image-preview" style="background-image: url('<?php echo base_url('/assets/img/logo/' . $profile->logo) ?>');background-size: cover;
    background-position: center center;">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>