  
    <div class="py-2 bg-light">
    </div>
    <header class="site-navbar py-2 js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="d-flex align-items-center">
          <div class="site-logo">
            <a href="<?php echo base_url()?>" class="d-block">
              <img src="<?php echo base_url('assets/') ?>Frontend/images/logo.jpg" alt="Image" class="img-fluid">
            </a>
          </div>
          <div class="mr-auto">
            <nav class="site-navigation position-relative text-right" role="navigation">
              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li class="active">
                  <a href="<?php echo base_url()?>beranda" class="nav-link text-left">Beranda</a>
                </li>
                <li class="has-children">
                  <a href="#" class="nav-link text-left">Profil</a>
                  <ul class="dropdown">
                    <li><a href="<?php echo base_url()?>">Visi Misi</a></li>
                    <li><a href="<?php echo base_url()?>">Tugas dan Fungsi</a></li>
                    <li><a href="<?php echo base_url()?>">Tujuan dan Sasaran</a></li>
                    <li><a href="<?php echo base_url()?>">Struktur Organisasi</a></li>
                    <li><a href="<?php echo base_url()?>">Kepegawaian</a></li>
                  </ul>
                </li>
                <li>
                  <a href="<?= base_url('Front/Berita') ?>" class="nav-link text-left">Berita</a>
                </li>
                <li class="has-children">
                  <a href="#" class="nav-link text-left">Informasi</a>
                  <ul class="dropdown">
                    <li><a href="<?php echo base_url()?>">Penumuman</a></li>
                    <li><a href="<?php echo base_url()?>">Agenda</a></li>
                  </ul>
                </li>
                <li class="has-children">
                  <a href="#" class="nav-link text-left">Galeri</a>
                  <ul class="dropdown">
                    <li><a href="<?php echo base_url()?>">Album Foto</a></li>
                    <li><a href="<?php echo base_url()?>">Video Kegiatan</a></li>
                  </ul>
                </li>
                <li>
                    <a href="<?php echo base_url()?>" class="nav-link text-left">Tentang Kami</a>
                </li>
                <?php
		// print_r($menu);
		?>
              </ul>                                                                                                                                                                                                                                                                                          </ul>
            </nav>

          </div>
          <div class="ml-auto">
            <div class="social-wrap">
              <a href="#"><span class="icon-facebook"></span></a>
              <a href="#"><span class="icon-twitter"></span></a>
              <!-- <a href="#"><span class="icon-linkedin"></span></a> -->

              <a href="#" class="d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
                class="icon-menu h3"></span></a>
            </div>
          </div>
         
        </div>
      </div>

    </header>