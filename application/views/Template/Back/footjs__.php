<!-- JS Libraies -->
<script src="<?php echo base_url('assets/') ?>Backend/libraries/jquery.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/popper.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/tooltip.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/nicescroll/jquery.nicescroll.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/moment.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/datatables/datatables.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/datatables/Select-1.2.4/js/dataTables.select.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/jquery-selectric/jquery.selectric.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/upload-preview/assets/js/jquery.uploadPreview.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/summernote/summernote-bs4.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/libraries/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>

<!-- General JS Scripts -->
<script src="<?php echo base_url('assets/') ?>Backend/js/stisla.js"></script>

<!-- Template JS File -->
<script src="<?php echo base_url('assets/') ?>Backend/js/scripts.js"></script>
<script src="<?php echo base_url('assets/') ?>Backend/js/custom.js"></script>
<?php if (!empty($script))
        foreach ($script as $val) : ?>
        <script src="<?= base_url('assets/Backend/page/' . $val) ?>"></script>
<?php endforeach ?>