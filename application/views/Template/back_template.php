<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('Template/Back/headcss__') ?>

<body>
  <div id="app">
    <div class="main-wrapper">

    <?php $this->load->view('Template/Back/navbar__') ?>

      <div class="main-sidebar">
        <?php $this->load->view('Template/Back/sidebar__') ?>
      </div>

      <!-- Main Content -->
      <!-- <div class="main-content"> -->
      
        <?php $this->load->view($content) ?>
        
      <!-- </div> -->
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2018 <div class="bullet"></div> Design By <a href="https://nauval.in/">Muhamad Nauval Azhar</a>
        </div>
        <div class="footer-right">
          2.3.0
        </div>
      </footer>
    </div>
  </div>

  <?php $this->load->view('Template/Back/footjs__') ?>
</body>
</html>
