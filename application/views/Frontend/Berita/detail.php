<div class="site-section ftco-subscribe-1 site-blocks-cover pb-4"
	style="background-image: url(<?php echo base_url('assets/') ?>Frontend/images/hero_1.jpg)">
	<div class="container">
		<div class="row align-items-end">
			<div class="col-lg-7">
				<h2 class="mb-0">Semua Berita</h2>
			</div>
		</div>
	</div>
</div>


<div class="custom-breadcrumns border-bottom">
	<div class="container">
		<a href="#">Home</a>
		<span class="mx-3 icon-keyboard_arrow_right"></span>
		<span class="current">Semua Berita</span>
	</div>
</div>

<div class="site-section">
	<div class="container">
		<div class="row mb-5">
			<div class="col-lg-12 mb-lg-0 mb-4">
				<img src=<?php echo base_url('assets/img/berita/' . $detail->gambar_berita) ?> alt="Image" class="img-fluid">
			</div>
			<div class="col-lg-12 ml-auto align-self-center">
				<h2 class="section-title-underline mb-5">
					<span><?php echo $detail->judul_berita ?></span>
				</h2>
				<p><?php echo $detail->isi_berita ?></p>
				<p><?= date("d F Y", strtotime($detail->tanggal_berita)); ?></p>

			</div>
		</div>

		
	</div>
</div>
