<div class="news-updates">
    <div class="container">

        <div class="row">
            <div class="col-lg-9">
                <div class="section-heading">
                    <h2 class="text-black">Berita</h2>
                    <a href="#">Semua Berita</a>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="post-entry-big">
                            <a href="news-single.html" class="img-link"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/blog_large_1.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#">June 6, 2019</a>
                                    <span class="mx-1">/</span>
                                    <a href="#">Admission</a>, <a href="#">Updates</a>
                                </div>
                                <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning
                                        Session</a></h3>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">
                        <?php foreach ($berita as $key => $value) : ?>
                        <div class="post-entry-big horizontal d-flex mb-4">
                            <a href="<?php echo base_url('Berita/' . $value->slug) ?>"  class="img-link mr-4"><img
                                    src=<?php echo base_url('assets/img/berita/' . $value->gambar_berita) ?> alt="Image"
                                    class="img-fluid"></a>
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#"><?= date("d F Y", strtotime($value->tanggal_berita)); ?></a>
                                    <span class="mx-1">/</span>
                                    <a href="#">Admission</a>, <a href="#">Updates</a>
                                </div>
                                <h3 class="post-heading"><a href="<?php echo base_url('Berita/' . $value->slug) ?>"><?php echo $value->judul_berita ?></a></h3>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <!-- <div class="post-entry-big horizontal d-flex mb-4">
                            <a href="news-single.html" class="img-link mr-4"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/blog_1.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#">June 6, 2019</a>
                                    <span class="mx-1">/</span>
                                    <a href="#">Admission</a>, <a href="#">Updates</a>
                                </div>
                                <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning
                                        Session</a></h3>
                            </div>
                        </div>

                        <div class="post-entry-big horizontal d-flex mb-4">
                            <a href="news-single.html" class="img-link mr-4"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/blog_2.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#">June 6, 2019</a>
                                    <span class="mx-1">/</span>
                                    <a href="#">Admission</a>, <a href="#">Updates</a>
                                </div>
                                <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning
                                        Session</a></h3>
                            </div>
                        </div>

                        <div class="post-entry-big horizontal d-flex mb-4">
                            <a href="news-single.html" class="img-link mr-4"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/blog_1.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="post-content">
                                <div class="post-meta">
                                    <a href="#">June 6, 2019</a>
                                    <span class="mx-1">/</span>
                                    <a href="#">Admission</a>, <a href="#">Updates</a>
                                </div>
                                <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning
                                        Session</a></h3>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="section-heading">
                    <h2 class="text-black">Pengumuman</h2>
                    <a href="#">Semua Pengumuman</a>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
                <div class="post-entry-big horizontal d-flex mb-4">
                    <div class="post-content">
                        <h3 class="post-heading"><a href="news-single.html">Campus Camping and Learning Session</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>