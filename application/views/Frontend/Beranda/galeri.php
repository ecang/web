<div class="site-section">
    <div class="container">


        <div class="row justify-content-center text-center">
            <div class="col-lg-6">
                <h2 class="section-title-underline mb-3">
                    <span>Galery Kegiatan</span>
                </h2>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="owl-slide-3 owl-carousel">
                    <div class="course-1-item">
                        <figure class="thumnail">
                            <a href="course-single.html"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/course_1.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="price">$99.00</div>
                            <div class="category">
                                <h3>Mobile Application</h3>
                            </div>
                        </figure>
                    </div>

                    <div class="course-1-item">
                        <figure class="thumnail">
                            <a href="course-single.html"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/course_2.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="price">$99.00</div>
                            <div class="category">
                                <h3>Web Design</h3>
                            </div>
                        </figure>

                    </div>

                    <div class="course-1-item">
                        <figure class="thumnail">
                            <a href="course-single.html"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/course_3.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="price">$99.00</div>
                            <div class="category">
                                <h3>Arithmetic</h3>
                            </div>
                        </figure>

                    </div>

                    <div class="course-1-item">
                        <figure class="thumnail">
                            <a href="course-single.html"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/course_4.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="price">$99.00</div>
                            <div class="category">
                                <h3>Mobile Application</h3>
                            </div>
                        </figure>

                    </div>

                    <div class="course-1-item">
                        <figure class="thumnail">
                            <a href="course-single.html"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/course_5.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="price">$99.00</div>
                            <div class="category">
                                <h3>Web Design</h3>
                            </div>
                        </figure>

                    </div>

                    <div class="course-1-item">
                        <figure class="thumnail">
                            <a href="course-single.html"><img
                                    src="<?php echo base_url('assets/') ?>Frontend/images/course_6.jpg" alt="Image"
                                    class="img-fluid"></a>
                            <div class="price">$99.00</div>
                            <div class="category">
                                <h3>Mobile Application</h3>
                            </div>
                        </figure>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>