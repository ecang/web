<!-- <div class="site-section"> -->
  <div class="container">
    <div class="row mb-5 justify-content-center text-center">
      <div class="col-lg-4">
        <h2 class="section-title-underline mb-5">
          <span>Data Kelurahan</span>
        </h2>
      </div>
    </div>
    <div class="row">
        <?php foreach ($kelurahan as $key => $val) : ?>
                <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
                    <div class="feature-1 border">
                        <div class="icon-wrapper bg-light">
                            <span><img class="card-img-top" src="<?php echo base_url('assets/img/logo/' . getProfile()->logo) ?>" alt="<?= getProfile()->profil_nama ?>"></span>
                        </div>
                        <div class="feature-1-content">
                            <h3 class="card-title"><?= $val->nama_kelurahan ?></h3>
                            <p class="card-text"><?= $val->alamat_kelurahan ?></p>
                            <p><a href="#" class="btn btn-primary px-4 rounded-0">Lihat</a></p>
                        </div>
                    </div>
                </div>
        <?php endforeach ?>
      <!-- <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">

        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-mortarboard text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Personalize Learning</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-school-material text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Trusted Courses</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Tools for Students</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Tools for Students</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Tools for Students</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Tools for Students</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 mb-3 mb-lg-5">
        <div class="feature-1 border">
          <div class="icon-wrapper bg-primary">
            <span class="flaticon-library text-white"></span>
          </div>
          <div class="feature-1-content">
            <h2>Tools for Students</h2>
            <p><a href="#" class="btn btn-primary px-4 rounded-0">Learn More</a></p>
          </div>
        </div>
      </div> -->
    </div>
  </div>
<!-- </div> -->