<?php
class M_menu extends CI_Model
 {
     function __construct()
     {
         parent::__construct();
     }
     
     // membaut menu submenu dinamis
     function getMenu($parent,$hasil){
		$this->db->from('tb_menu');		
         $w = $this->db->where('id_parent', $parent )->get();
         foreach($w->result() as $h)
         {
			$this->db->from('tb_menu');
			 $cek_parent= $this->db->where('id_parent', $h->id_menu )->get();
         if(($cek_parent->num_rows())>0){
				$hasil .= '<li class="has-children"><a href="#" class="nav-link text-left">'.$h->nama_menu.'</a> ';
				}
            else {
					$hasil.='<li><a href="#" class="nav-link text-left">'.$h->nama_menu.'</a></li>';
				}
				$hasil .='<ul class="dropdown">';
				$hasil = $this->getMenu($h->id_menu,$hasil);
				$hasil .='</ul>';		
				$hasil .= "</li></li>";
         }
         
         return str_replace('<ul class="dropdown"></ul>','',$hasil);
	 }     
	 
     
     // fungsi untuk menampilkan menu yang di klik
     public function read($id_menu){
		$this->db->where('id_menu',$id_menu);
		$sql_menu=$this->db->get('menu');
			if($sql_menu->num_rows()==1){
				return $sql_menu->row_array();	 
			}	 
		}

    //untuk menyimpan menu
	public function save(){
	if($this->input->post('parent_id')) {$parent_id=$this->input->post('parent_id') ;}else { $parent_id=0;}
	$data_menu=array(
	'parent_id'=>$parent_id,
	'menu'=>$this->input->post('menu'),
	'menu_order'=>$this->input->post('order_menu'),
	'isi_menu'=>$this->input->post('isi_menu')
	);
	$this->db->insert('menu',$data_menu);
	}
	
	//untuk meng-update menu
	public function update($kode){
	$data_menu=array(
	'id_menu'=>$kode,
	'menu'=>$this->input->post('menu'),
	'menu_order'=>$this->input->post('order_menu'),
	'isi_menu'=>$this->input->post('isi_menu')
	);
	$this->db->where('id_menu',$kode);	
	$this->db->update('menu',$data_menu);

	}

}		
 ?>
