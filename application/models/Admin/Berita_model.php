<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Berita_model extends MY_Model
{
    public $table = 'tb_berita';
    public $primary_key = 'id_berita';
    public $column_order = array(null, 'judul_berita', 'tanggal_berita');
    public $column_search = array('page_name');
    public $order = array('created_at' => 'desc');
    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file Berita_mode;.php */
