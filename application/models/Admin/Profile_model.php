<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Profile_model extends CI_Model
{
    public $table = 'tb_profil';
    public function getTable()
    {
        return $this->db->get($this->table)->row();
    }
    public function update($data)
    {
        return $this->db->update($this->table, $data);
    }
}

/* End of file Profile_model.php */
