<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Sub_Menu_model extends MY_Model
{

    public $table = 'tb_sub_menu';
    public $primary_key = 'id_sub_menu';

    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file Sub_Menu_model.php */
