<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends MY_Model
{
    public $table = 'tb_menu';
    public $primary_key = 'id_menu';
    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file Menu_model.php */
