<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Kelurahan_model extends MY_Model
{
    public $table = 'tb_kelurahan';
    public $primary_key = 'id_kelurahan';
    public $column_order = array(null, 'judul_berita', 'tanggal_berita');
    public $column_search = array('page_name');
    public $order = array('created_at' => 'desc');
    public function __construct()
    {
        parent::__construct();
    }
}

/* End of file Kelurahan_model.php */
