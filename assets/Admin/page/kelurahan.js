"use strict";
if (jQuery().summernote) {
    $("#keterangan").summernote({
        dialogsInBody: true,
        minHeight: 250,
    });
}
var postForm = function () {
    $('textarea[name="keterangan"]').html($("#keterangan").code());
};
var calback = $('#callback-content').html();
if (calback != "") {
    $("#keterangan").summernote("code", calback);
}