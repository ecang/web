"use strict";
$(function () {
    var table = $("#dataTable-anggota").dataTable({
        serverSide: true,
        processing: true,
        paging: true,
        ordering: true,
        ajax: {
            url: $("#dataTable-anggota").data("url"),
            type: "POST"
        },
        columnDefs: [{
            className: "text-center",
            targets: 0
        }]
    });
});