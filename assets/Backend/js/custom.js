/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";
var bases = location.origin;


$.uploadPreview({
    input_field: "#image-upload",   // Default: .image-upload
    preview_box: "#image-preview",  // Default: .image-preview
    label_field: "#image-label",    // Default: .image-label
    label_default: "Choose File",   // Default: Choose File
    label_selected: "Change File",  // Default: Change File
    no_label: false,                // Default: false
    success_callback: null          // Default: null
});
// if (jQuery().summernote) {
//     $(".summernote-berita").summernote({
//         dialogsInBody: true,
//         minHeight: 250,
//         callbacks: {
//             onImageUpload: function (image) {
//                 uploadImage(image[0]);
//                 console.log("upload");
//             },
//             onMediaDelete: function (target) {
//                 deleteImage(target[0].src);
//             }
//         }
//     });
// }

$(function () {
    var table = $("#dataTables").dataTable({
        processing: true,
        paging: true,
        ordering: true,
        ajax: {
            url: $("#dataTables").data("url"),
            type: "POST"
        },
        columnDefs: [{
            className: "text-center",
            targets: 0
        }]
    });
    if ($(".tagsberita").length) {
        console.log(bases);
        $('.tagsberita').select2({
            width: 'resolve',
            tags: true,
            allowClear: true,
            ajax: {
                type: 'POST',
                dataType: 'json',
                url: $('.tagsberita').data('url'),
                delay: 800,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                success: function (data) {
                    return {
                        results: data
                    }
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
            }

        });
    }
});
